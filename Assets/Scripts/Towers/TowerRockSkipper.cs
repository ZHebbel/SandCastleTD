﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerRockSkipper : TowerBase {
    void Start() {
        mIsOnShotCooldown = false;
        mCurrentTarget = null;
        towerType = TowerType.RockSkipper;
    }

    void Update() {
        TargetUnit(CheckForEnemy());
        //Debug.Log(mCurrentTarget);
        AttackTarget();
        if(mCurrentTarget != null)
        {
            if (mCurrentTarget.GetComponent<UnitBase>().Health <= 0)
            {
                mCurrentTarget = null;
            }
        }
        
    }

    public override void TargetUnit(GameObject unit)
    {
        mCurrentTarget = unit;
    }

     public override void AttackTarget()
    {
        if(!mIsOnShotCooldown)
        {
            //Debug.Log("Shot");
            if(mCurrentTarget != null)
            {
				LaunchProjectile(mCurrentTarget.GetComponent<UnitBase>());
				//mCurrentTarget.GetComponent<UnitBase>().Health -= mDamage;
            }
            //Debug.Log(mCurrentTarget.GetComponent<UnitBase>().Health);
            mIsOnShotCooldown = true;
            StartCoroutine(ResetShot());
        }
    }

	public override void LaunchProjectile(UnitBase target)
	{
		ProjectilePool.Instance.ActivateSandBall(gameObject.transform.position, target);
	}
		

    IEnumerator ResetShot()
    {
        yield return new WaitForSeconds(mAttackSpeed);
        mIsOnShotCooldown = false;
    }

    GameObject CheckForEnemy()
    {
        GameObject closest = null;
        float closestPos = mRadius;
        if (mCurrentTarget == null)
        {
            //Debug.Log("CheckEnemy 1");
            GameObject tmp = null;
            Vector3 towerPos = this.gameObject.GetComponent<Transform>().position;
            if(UnitManager.instance.mArmy.Count > 0)
            {
                foreach (GameObject u in UnitManager.instance.mArmy)
                {
                    //Debug.Log("CheckEnemy 2");
                    Vector3 uPos = u.GetComponent<Transform>().position;
                    float dist = Vector3.Distance(uPos, towerPos);
                    if (dist < mRadius)
                    {
                        //Debug.Log("CheckEnemy 3");
                        if (dist < closestPos)
                        {
                            //Debug.Log("CheckEnemy 4");
                            closestPos = dist;
                            tmp = u;
                        }
                    }
                }
                closest = tmp;
                tmp = null;
            }
        }
        return closest;
    }
}
