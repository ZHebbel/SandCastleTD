﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemController : MonoBehaviour
{
    public Button sampleButton;                         // sample button prefab
    private List<ContextMenuItem> rockSlotMenu;        // list of items in the empty menu
    private List<ContextMenuItem> towerSlotMenu;        // list of items in the tower menu

    private Collider2D rockCollider;

    private int RockSkipperCost;
    private int MudSlingerCost;
    private int SeaWeedSlingshotCost;

    void Awake()
    {
        RockSkipperCost = 25;
        MudSlingerCost = 50;
        SeaWeedSlingshotCost = 40;
        rockSlotMenu = new List<ContextMenuItem>();
        towerSlotMenu = new List<ContextMenuItem>();
        Action<Image> tower1 = new Action<Image>(Tower1Action);
        Action<Image> tower2 = new Action<Image>(Tower2Action);
        Action<Image> tower3 = new Action<Image>(Tower3Action);
        //Action<Image> upgrade = new Action<Image>(UpgradeAction);
        Action<Image> sell = new Action<Image>(SellAction);
        Action<Image> cancel = new Action<Image>(CancelAction);

        rockSlotMenu.Add(new ContextMenuItem("Rock Skipper ("+RockSkipperCost+")", sampleButton, tower1));
        rockSlotMenu.Add(new ContextMenuItem("Mud Slinger (" + MudSlingerCost + ")", sampleButton, tower2));
        rockSlotMenu.Add(new ContextMenuItem("SeaWeed Slingshot (" + SeaWeedSlingshotCost+ ")", sampleButton, tower3));
        rockSlotMenu.Add(new ContextMenuItem("Cancel", sampleButton, cancel));

        //towerSlotMenu.Add(new ContextMenuItem("Upgrade", sampleButton, upgrade));
        towerSlotMenu.Add(new ContextMenuItem("Sell (50%)", sampleButton, sell));
        towerSlotMenu.Add(new ContextMenuItem("Cancel", sampleButton, cancel));
    }

    void Start()
    {
        rockCollider = this.gameObject.GetComponent<Collider2D>();

    }

    void OnMouseOver()
    {
        // TODO: Change tags to check enum once implemented
        if (Input.GetMouseButtonDown(0) && gameObject.tag == "Tower" && LevelManager.Instance.canBuy == true)
        {
            rockCollider.enabled = false;
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            ContextMenu.Instance.CreateContextMenu(towerSlotMenu, new Vector2(pos.x, pos.y));
        }
        else if (Input.GetMouseButtonDown(0) && gameObject.tag == "Rock" && LevelManager.Instance.canBuy == true)
        {
            rockCollider.enabled = false;
            Vector3 pos = Camera.main.WorldToScreenPoint(transform.position);
            ContextMenu.Instance.CreateContextMenu(rockSlotMenu, new Vector2(pos.x, pos.y));
        }

        if (Input.GetMouseButtonDown(1) && gameObject.tag == "Tower")
        {
            gameObject.GetComponent<DrawRadius>().canDraw = !gameObject.GetComponent<DrawRadius>().canDraw;
            Debug.Log("Radius is " + gameObject.GetComponent<DrawRadius>().canDraw);
        }
    }

    // TODO: Add button functionality; currently they just print to console.s
    void Tower1Action(Image contextPanel)
    {
        Debug.Log("Tower 1 purchased");
        if(this != null)
        {
            TowerManager.instance.BuyTower(TowerBase.TowerType.RockSkipper, this.gameObject);
            rockCollider.enabled = true;
            //GameManager.Instance.playerSand -= TowerManager.instance.
        }

        Destroy(contextPanel.gameObject);
    }

    void Tower2Action(Image contextPanel)
    {
        Debug.Log("Tower 2 purchased");
        if (this != null)
        {
            TowerManager.instance.BuyTower(TowerBase.TowerType.MudSlinger, this.gameObject);
            rockCollider.enabled = true;

        }
        Destroy(contextPanel.gameObject);
    }

    void Tower3Action(Image contextPanel)
    {
        Debug.Log("Tower 3 purchased");
        if (this != null)
        {
            TowerManager.instance.BuyTower(TowerBase.TowerType.SeaWeedSlingshot, this.gameObject);
            rockCollider.enabled = true;

        }
        Destroy(contextPanel.gameObject);
    }

    //void UpgradeAction(Image contextPanel)
    //{
    //    Debug.Log("Tower upgraded");
    //    Destroy(contextPanel.gameObject);
    //}

    void SellAction(Image contextPanel)
    {
        Debug.Log("Tower sold");
        if (this != null)
        {
            TowerManager.instance.SellTower(TowerBase.TowerType.Rock, this.gameObject);
            rockCollider.enabled = true;
        }
        Destroy(contextPanel.gameObject);
    }

    void CancelAction(Image contextPanel)
    {
        Debug.Log("Cancel");
        rockCollider.enabled = true;
        Destroy(contextPanel.gameObject);
    }
}
