﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	private static GameManager _instance;
	public static GameManager Instance
	{
		get
		{
			return _instance;
		}
	}

    public int playerHealth = 5;
	bool isAlive = true;
    public int waveCount = 0;
    public int playerSand = 200;

	[HideInInspector]public int CurrentLevel = 1;

	public enum GameMode{
		Menu,
		Level
	}

	public GameMode currentMode = GameMode.Menu;

	// Use this for initialization
	void Awake () {
		GameObject[] gameManagers = GameObject.FindGameObjectsWithTag("GameManager");
		if (gameManagers.Length > 1)
		{
			Destroy(gameObject);
		}else
		{
			_instance = this;
			DontDestroyOnLoad(gameObject);
		}
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (currentMode == GameMode.Level)
        {
			if (isAlive)
			{
				if (playerHealth <= 0)
				{
					UIManager.instance.DisableUIPanel(UIManager.instance.gamePanel);
					UIManager.instance.EnableUIPanel(UIManager.instance.endPanel);
					UnitManager.instance.ReturnAllToPool();
				}
			}
        }
	}

	void EnterGame()
	{
		currentMode = GameMode.Level;
		//SceneManager.LoadScene("CS_PathfindingTest");
	}

	void GoToMenu()
	{
		currentMode = GameMode.Menu;
		//SceneManager.LoadScene("main");
	}

	public void SetGameMode(GameMode newMode)
	{
		switch (newMode)
		{
			case GameMode.Menu:
				GoToMenu();
				break;
			case GameMode.Level:
				EnterGame();
				break;
			default:
				break;
		}
	}

	public void Lose()
	{
		Debug.Log("LOSE!!!");
		isAlive = false;
		SoundManager.Instance.PlayLose();
		UnitManager.instance.ResetUnitsKilled();
	}

	public void Win()
	{
		Debug.Log("WIN!!!");
		SoundManager.Instance.PlayWin();
		LevelManager.Instance.StartCleanupOnDelay();
		UnitManager.instance.ResetUnitsKilled();
	}

	public void Reset()
	{
		playerHealth = 5;
		isAlive = true;
		playerSand = 200;
		waveCount = 0;
	}
}
