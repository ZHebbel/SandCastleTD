﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
	private static LevelManager _instance;
	public static LevelManager Instance
	{
		get
		{
			return _instance;
		}
	}

	public enum LevelState
	{
		Intro,
		Setup,
		Defend,
		Cleanup
	}
	private LevelState currentLevelState = LevelState.Intro;

	public int StagesInLevel;
	public int currentStage = 0;

	public GameObject background;

	public List<Sprite> maps;
	public List<Path> paths;

	public GameObject tideSprite;
	public GameObject castle;

    public bool canBuy { get; set; }

    List<UnitManager.UnitType> testArmy = new List<UnitManager.UnitType> {
        UnitManager.UnitType.Droplet,
        UnitManager.UnitType.Droplet,
        UnitManager.UnitType.Droplet,
        UnitManager.UnitType.Turtle};

    //----------------------------------------------------------------------------------------------------
    void Awake() {
		_instance = this;
	}

	void Start()
	{
		StartIntroSequence();
	}
	
	void Update () {
		
	}

	public void SetLevelState(LevelState nextState)
	{
		ExitState(currentLevelState);
		EnterLevelState(nextState);
	}

	private void ExitState(LevelState state)
	{
		switch (state)
		{
			case LevelState.Intro:
                UIManager.instance.EnableUIPanel(UIManager.instance.gamePanel);
                break;
			case LevelState.Setup:
				break;
			case LevelState.Defend:
                UnitManager.instance.CanSpawn = false;
				break;
            case LevelState.Cleanup:
				break;
			default:
				break;
		}
	}

	private void EnterLevelState(LevelState state)
	{
		currentLevelState = state;
		switch (state)
		{
			case LevelState.Intro:
                canBuy = false;
                UIManager.instance.DisableUIPanel(UIManager.instance.gamePanel);
				tideSprite.SetActive(true);
                tideSprite.GetComponent<Animator>().Play("Tide_Animation");
                UnitManager.instance.ReturnAllToPool();
                UnitManager.instance.SetUnitsForNextRound(testArmy, 1.0f);
                UnitManager.instance.CanSpawn = false;
				break;
            case LevelState.Setup:
                GameManager.Instance.waveCount++;
                canBuy = true;
                break;
			case LevelState.Defend:
                UnitManager.instance.CanSpawn = true;
                break;
			case LevelState.Cleanup:
				break;
			default:
				break;
		}
	}

	public void SwitchLevelPaths()
	{
		background.GetComponent<SpriteRenderer>().sprite = maps[currentStage];
		UnitManager.instance.SpawnPosition = paths[currentStage].startingPoints[0];
		UnitManager.instance.EndPosition = paths[currentStage].endPoint;
		castle.transform.position = paths[currentStage].castlePosition;
	}

	public Waypoint GetSpawnPosition()
	{
		int count = paths[currentStage].startingPoints.Count;
		if (count == 1)
		{
			return paths[currentStage].startingPoints[0];
		}else
		{
			return paths[currentStage].startingPoints[Random.Range(0, count)];
		}
	}

	public void StartIntroSequence()
	{
		StopCoroutine(PlayIntroSequence());
		StartCoroutine(PlayIntroSequence());
	}

	public bool GoNextStage()
	{
		++currentStage;
		//if (currentStage < StagesInLevel)
		//{
		//	StartIntroSequence();
		//	return true;
		//}
		//else
		//{
		//	currentStage = 0;
		////load next scene
		//	return false;
		//}
		if (currentStage >= StagesInLevel)
		{
			currentStage = 0;
		}
		StartIntroSequence();
		return true; //it's always true, now
	}

	public void StartCleanupOnDelay()
	{
		StartCoroutine(DelayThenCleanup());
	}

	IEnumerator PlayIntroSequence()
	{
		//Make sure state is set to intro
		SetLevelState(LevelState.Intro);
		SoundManager.Instance.PlayWave();
		yield return new WaitForSeconds(4);
		//switch level paths while tide is covering them
		SwitchLevelPaths();
		yield return new WaitForSeconds(5);
		//enter setup state
		SetLevelState(LevelState.Setup);
		yield return new WaitForSeconds(5);
		//enter defend state
		SetLevelState(LevelState.Defend);
	}

	IEnumerator DelayThenCleanup()
	{
		//Make sure state is set to intro
		SetLevelState(LevelState.Cleanup);
		yield return new WaitForSeconds(3);
		GoNextStage();
	}
}
