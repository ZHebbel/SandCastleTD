﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitTurtle : UnitBase {

    void Start()
    {
        mUnitType = UnitType.Turtle;
        mPosition = gameObject.GetComponent<Transform>();
        mIsInCastle = false;
    }

    void Update()
    {
        if (Health >= 0)
        {
            MoveTo(NextPosition);
            if (Health < StartingHealth / 3)
            {
                mAnimator.SetInteger("HurtLevel", 2);
            }
            else if (Health < StartingHealth * 2 / 3)
            {
                if (Health < StartingHealth / 3)
                {
                    mAnimator.SetInteger("HurtLevel", 1);
                }
            }

        }
        else
        {
            Die();
        }
    }

    public override void SetPosition(Vector3 position)
    {
        mPosition.Translate(position);
    }

    public override void MoveTo(Vector3 position)
    {
        if (mPosition.position != position)
        {
            mPosition.position = Vector3.MoveTowards(mPosition.position, NextPosition, MoveSpeed * Time.deltaTime);
        }
        else
        {
            SetNextWaypoint(mWaypoint);
        }
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "CastleZone")
        {
            if (GameManager.Instance.playerHealth >= 1)
            {
                EnterCastle();
            }
        }
    }

    public override void EnterCastle()
    {
        Debug.Log("Reached castle");
        mIsInCastle = true;
        UnitManager.instance.canGiveSand = false;
        UnitManager.instance.RemoveUnit(this.gameObject);
        UnitManager.instance.canGiveSand = true;
		SoundManager.Instance.PlaySplash();
		GameManager.Instance.playerHealth--;
		UnitManager.instance.SpendUnit();
    }

    public override void SetNextWaypoint(Waypoint next)
    {
        NextPosition = next.GetNextWaypoint().transform.position;
        mWaypoint = next.GetNextWaypoint();
    }
}
