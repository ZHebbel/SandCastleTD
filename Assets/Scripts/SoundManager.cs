﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	private static SoundManager _instance;
	public static SoundManager Instance
	{
		get { return _instance; }
	}

	public AudioClip ambientClick;
	public AudioClip buttonClick;
	public AudioClip bloop;
	public AudioClip sandCrunch;
	public AudioClip water;
	public AudioClip waterDrip;
	public AudioClip waveBreak;
	public AudioClip splash;
	public AudioClip cheer;
	public AudioClip lose;
	public AudioClip swoosh;

	public AudioClip music1;

	public AudioSource clipPlayer1; //UI
	public AudioSource clipPlayer2; //Background noise
	public AudioSource clipPlayer3; //Construction, Wave
	//Unit spawn, death, and construction will be on units
	public AudioSource musicPlayer1; //Background music

	

	void Awake()
	{
		if (_instance == null)
		{
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			Destroy(this.gameObject);
		}
		
	}


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayClick1()
	{
		clipPlayer1.PlayOneShot(ambientClick);
	}

	public void PlayClick2()
	{
		clipPlayer1.PlayOneShot(buttonClick);
	}

	public void PlaySandCrunch()
	{
		clipPlayer3.PlayOneShot(sandCrunch);
	}

	public void PlayWave()
	{
		clipPlayer3.PlayOneShot(waveBreak);
	}

	public void PlaySplash()
	{
		clipPlayer2.PlayOneShot(splash);
	}

	public void PlayKillUnit()
	{
		//because the audiosource on the player keeps getting destroyed before it can play this
		clipPlayer3.PlayOneShot(waterDrip);
	}

	public void PlayWin()
	{
		clipPlayer2.PlayOneShot(cheer);
	}

	public void PlayLose()
	{
		clipPlayer2.PlayOneShot(lose);
	}
	public void PlayTransitionSwoosh()
	{
		clipPlayer2.PlayOneShot(swoosh);
	}
}
