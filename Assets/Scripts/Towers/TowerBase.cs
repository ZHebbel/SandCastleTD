﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBase : MonoBehaviour {
    public int mDamage;
    public float mAttackSpeed;
    public GameObject mCurrentTarget;
    public float mRadius;
    public int mCost;

    public enum TowerType
    {
        Rock,
        RockSkipper,
        MudSlinger,
        SeaWeedSlingshot
    }

    public TowerType towerType;

    protected int mHealth;
    protected bool mIsOnShotCooldown;
    
    public virtual void TargetUnit(GameObject unit) { }
    public virtual void AttackTarget() { }
	public virtual void LaunchProjectile(UnitBase target) { }

}
