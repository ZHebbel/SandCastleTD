﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerManager : MonoBehaviour {

    private static TowerManager _instance;
    public static TowerManager instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private List<TowerBase> mTowers;
    public GameObject purchaseRockSkipper;
    public GameObject purchaseMudSlinger;
    public GameObject purchaseSeaWeedSlingshot;
    public GameObject purchaseRock;
    
    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
		
	}

    public void RemoveTower(TowerBase tower)
    {
        Destroy(tower);
    }

    public void AddTower(TowerBase tower)
    {
        mTowers.Add(tower);
    }

    public void DeductCost(TowerBase tower)
    {
        GameManager.Instance.playerSand -= tower.mCost;
    }

    public void RefundTower(TowerBase tower)
    {
        GameManager.Instance.playerSand += tower.mCost/2;
    }

    public void BuyTower(TowerBase.TowerType towerType, GameObject tower)
    {
        switch(towerType)
        {
            case TowerBase.TowerType.RockSkipper:
                if (GameManager.Instance.playerSand > purchaseRockSkipper.GetComponent<TowerBase>().mCost)
                {
                    GameObject newTower = Instantiate(purchaseRockSkipper, tower.GetComponent<Transform>().position, tower.GetComponent<Transform>().rotation);
                    DeductCost(newTower.GetComponent<TowerBase>());
                    Destroy(tower);
                }

                break;
            case TowerBase.TowerType.MudSlinger:
                if (GameManager.Instance.playerSand > purchaseMudSlinger.GetComponent<TowerBase>().mCost)
                {
                    GameObject newTower = Instantiate(purchaseMudSlinger, tower.GetComponent<Transform>().position, tower.GetComponent<Transform>().rotation);
                    DeductCost(newTower.GetComponent<TowerBase>());
                    Destroy(tower);
                }

                break;
            case TowerBase.TowerType.SeaWeedSlingshot:
                if (GameManager.Instance.playerSand > purchaseSeaWeedSlingshot.GetComponent<TowerBase>().mCost)
                {
                    GameObject newTower = Instantiate(purchaseSeaWeedSlingshot, tower.GetComponent<Transform>().position, tower.GetComponent<Transform>().rotation);
                    DeductCost(newTower.GetComponent<TowerBase>());
                    Destroy(tower);
                }
                
                break;
            default:
                break;
        }
    }

    public void SellTower(TowerBase.TowerType towerType, GameObject tower)
    {
        switch (towerType)
        {
            case TowerBase.TowerType.Rock:
                GameObject newTower = Instantiate(purchaseRock, tower.GetComponent<Transform>().position, tower.GetComponent<Transform>().rotation);
                RefundTower(tower.GetComponent<TowerBase>());
                Destroy(tower);
                break;
            default:
                break;
        }
    }
}
