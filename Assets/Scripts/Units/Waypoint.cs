﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour {
	public List<Waypoint> nextWaypoints;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public Waypoint GetNextWaypoint()
	{
		int count = nextWaypoints.Count;
		Waypoint next = nextWaypoints[0];
		if (count > 1)
		{
			next = nextWaypoints[Random.Range(0, count)];
		}
		return next;
	}
}
