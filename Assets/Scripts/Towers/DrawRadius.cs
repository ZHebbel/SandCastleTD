﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawRadius : MonoBehaviour {

    public int segments;
    public float xradius;
    public float yradius;
    LineRenderer line;
    public bool canDraw { get; set; }

    void Start()
    {
        canDraw = true;
        segments = 36;
        xradius = this.gameObject.GetComponent<TowerBase>().mRadius/3;
        yradius = this.gameObject.GetComponent<TowerBase>().mRadius/3;

        line = gameObject.GetComponent<LineRenderer>();
        line.widthMultiplier = 0.05f;
        line.startColor = Color.green;
        line.endColor = Color.green;

        line.SetVertexCount(segments + 1);
        line.useWorldSpace = false;

        CreatePoints();
    }

    void OnGUI()
    {
        if(canDraw)
        {
            line.enabled = true;
        }
        else
        {
            line.enabled = false;
        }
    }

    void CreatePoints()
    {
        float x;
        float y;
        float z = 0f;

        float angle = 10f;

        for (int i = 0; i < (segments + 1); i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * xradius;
            y = Mathf.Cos(Mathf.Deg2Rad * angle) * yradius;

            line.SetPosition(i, new Vector3(x, y, z));

            angle += (360f / segments);
        }
    }
}
