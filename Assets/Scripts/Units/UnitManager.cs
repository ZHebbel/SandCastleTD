﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager : MonoBehaviour {

    private static UnitManager _instance;
    public static UnitManager instance
    {
        get { return _instance; } 
    }

    void Awake()
    {
        if(_instance == null) {
            _instance = this;
        }
        else {
            Destroy(this);
        }
    }

    public enum UnitType
    {
        Droplet,
        Crab,
        Turtle
    }
	private UnitType unitType;

	public Waypoint SpawnPosition;
    public Waypoint EndPosition;
    public int StartingUnitCount;
    public List<GameObject> mArmy;
    public float SpawnDelay;
    public bool canGiveSand; 

	public GameObject Droplet;
	public GameObject Crab;
	public GameObject Turtle;

	private int currentUnit;
	private int unitsKilled;
    private bool mSpawn;

    public bool CanSpawn
    {
        get { return mSpawn; }
        set { mSpawn = value; }
    }

    void Start () {
        mSpawn = false;
		//Array units = Enum.GetValues(typeof(UnitType));
		//System.Random random = new System.Random();
		//UnitType randomUnit = 0;
		//for (int i = 0; i < StartingUnitCount; ++i)
		//{
		//	randomUnit = (UnitType)units.GetValue(random.Next(units.Length));
		//	CreateUnit(randomUnit);
		//}
		currentUnit = 0;
        canGiveSand = false;
    }

    void Update()
    {
        if (mSpawn == true)
        {
            mSpawn = false;
            ActivateNextFromPool();
            if (currentUnit < StartingUnitCount)
            {
                //Reset currentUnit to 0 to continue spawning
                StartCoroutine(waitToSpawn());
            }
        }
        canGiveSand = true;
        // Debug.Log("Spawn " + mSpawn);
    }

	public void CreateUnit(UnitType unitType)
	{
		GameObject unit = null;
		//Create unit
		switch (unitType)
		{
			case UnitType.Droplet:
				unit = (GameObject)Instantiate(Droplet);
				mArmy.Add(unit);
				//Debug.Log("Droplet created");
				//Debug.Log("Army size: " + mArmy.Count);
				break;
			case UnitType.Crab:
				unit = (GameObject)Instantiate(Crab);
				mArmy.Add(unit);
				break;
			case UnitType.Turtle:
				unit = (GameObject)Instantiate(Turtle);
				mArmy.Add(unit);
				break;
			default:
				break;
		}
	}

	public void RemoveUnit(GameObject unit)
    {
        unit.gameObject.SetActive(false);
		Waypoint nextPosition = LevelManager.Instance.GetSpawnPosition();
        unit.gameObject.transform.position = nextPosition.transform.position;
        unit.gameObject.GetComponent<UnitBase>().SetNextWaypoint(nextPosition);
        if(currentUnit >= 1 && canGiveSand == true)
        {
            GameManager.Instance.playerSand += unit.GetComponent<UnitBase>().sandReward;
        }
        //Debug.Log("Unit returned to pool");
    }

    public void ReturnAllToPool()
    {
        int count = mArmy.Count;
        for (int i = 0; i < count; ++i)
        {
            mArmy[i].gameObject.SetActive(false);
			Waypoint nextPosition = LevelManager.Instance.GetSpawnPosition();
			mArmy[i].gameObject.transform.position = nextPosition.transform.position;
            mArmy[i].GetComponent<UnitBase>().SetNextWaypoint(nextPosition);

        }
    }

    public void ActivateNextFromPool()
    {
        int count = mArmy.Count;
        for (int i = 0; i < count; ++i)
        {
            if (mArmy[i] != null)
            {
                if (mArmy[i].gameObject.activeInHierarchy == false)
                {
                    mArmy[i].gameObject.SetActive(true);
					Waypoint nextPosition = LevelManager.Instance.GetSpawnPosition();
					mArmy[i].gameObject.transform.position = nextPosition.transform.position;
                    mArmy[i].GetComponent<UnitBase>().SetNextWaypoint(nextPosition);
					mArmy[i].GetComponent<UnitBase>().PlaySpawnSound();
					//i = count;
					++currentUnit;
                    break;
                }
            }
        }
    }

	public void ResetUnitsKilled()
	{
		unitsKilled = 0;
		currentUnit = 0;
		for (int i = 0; i < mArmy.Count; ++i)
		{
			mArmy[i].GetComponent<UnitBase>().Reset();
		}

	}

	public void KillUnit()
	{
		SpendUnit();
	}

	public void SpendUnit()
	{
		++unitsKilled;
		if (unitsKilled >= StartingUnitCount)
		{
			GameManager.Instance.Win();
		}
		Debug.Log(unitsKilled);
	}

    public void SetUnitsForNextRound(List<UnitManager.UnitType> unitArray, float spawnDelay)
    {
        mArmy.Clear();
        //foreach(UnitType uT in unitArray)
        //{
        //    CreateUnit(uT);
        //}
        int c = unitArray.Count;
        for(int i = 0; i < c; ++i)
        {
            CreateUnit(unitArray[i]);        
        }
        SpawnDelay = spawnDelay;
        StartingUnitCount = c;
    }

    IEnumerator waitToSpawn()
    {
        yield return new WaitForSeconds(SpawnDelay);
        mSpawn = true;
    }
}
