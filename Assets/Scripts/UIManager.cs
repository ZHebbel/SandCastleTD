﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance { get { return _instance; } }
    private static UIManager _instance;

    public GameObject menuPanel;
    public GameObject creditsPanel;
    public GameObject gamePanel;
    public GameObject pausePanel;
    public GameObject endPanel;
    public GameObject instructionsPanel;

    public Button playButton;
    public Button creditsButton;
    public Button instructionsButton;
    public Button backButton;
    public Button pauseButton;
    public Button quitButton;
    public Button resumeButton;
    public Button pauseRestartButton;
    public Button endRestartButton;

    public Text playerHealth;
    public Text playerSand;
    public Text waveCount;

    private bool mGameUpdate;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject); // there can be only one
        }
        else
        {
            _instance = this;
            SceneManager.sceneLoaded += OnLevelFinishedLoading;
        }
     
       
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("we did it");
		GameManager.Instance.Reset();

        if (GameManager.Instance.currentMode == GameManager.GameMode.Menu)
        {
            menuPanel = GameObject.Find("MenuPanel");
            creditsPanel = GameObject.Find("CreditsPanel");
            instructionsPanel = GameObject.Find("InstructionsPanel");

            playButton = GameObject.Find("PlayButton").GetComponent<Button>();
            creditsButton = GameObject.Find("CreditsButton").GetComponent<Button>();
            instructionsButton = GameObject.Find("InstructionsButton").GetComponent<Button>();
            backButton = GameObject.Find("BackButton").GetComponent<Button>();
            quitButton = GameObject.Find("MenuQuitButton").GetComponent<Button>();

            DisableUIPanel(instructionsPanel);
            DisableUIPanel(creditsPanel);

            playButton.onClick.AddListener(() => { SceneManager.LoadScene("CS_Map1", LoadSceneMode.Single); GameManager.Instance.SetGameMode(GameManager.GameMode.Level); });
            creditsButton.onClick.AddListener(() => { StartCoroutine(waitForCredits()); });
            instructionsButton.onClick.AddListener(() => { DisableUIPanel(menuPanel); EnableUIPanel(instructionsPanel); });
            backButton.onClick.AddListener(() => { DisableUIPanel(instructionsPanel); EnableUIPanel(menuPanel); });

        }

        else
        {
            gamePanel = GameObject.Find("GamePanel");
            pausePanel = GameObject.Find("PausePanel");
            endPanel = GameObject.Find("EndPanel");

            pauseButton = GameObject.Find("PauseButton").GetComponent<Button>();
            quitButton = GameObject.Find("GameQuitButton").GetComponent<Button>();
            resumeButton = GameObject.Find("ResumeButton").GetComponent<Button>();
            pauseRestartButton = GameObject.Find("PauseRestartButton").GetComponent<Button>();
            endRestartButton = GameObject.Find("EndRestartButton").GetComponent<Button>();

            playerHealth = GameObject.Find("HealthText").GetComponent<Text>();
            playerSand = GameObject.Find("WetSandText").GetComponent<Text>();
            waveCount = GameObject.Find("WaveText").GetComponent<Text>();

            DisableUIPanel(endPanel);
            DisableUIPanel(pausePanel);

            mGameUpdate = true;

            resumeButton.onClick.AddListener(() => DisableUIPanel(pausePanel));
            resumeButton.onClick.AddListener(() => TimeRestart());
            pauseButton.onClick.AddListener(() => EnableUIPanel(pausePanel));
            pauseButton.onClick.AddListener(() => TimePause());
            pauseRestartButton.onClick.AddListener(() => RestartGame());
            pauseRestartButton.onClick.AddListener(() => TimeRestart());
            endRestartButton.onClick.AddListener(() => RestartGame());
            quitButton.onClick.AddListener(() => { Application.Quit();});
        }

    }

    // Use this for initialization
    void Start()
    {
        mGameUpdate = false;
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (mGameUpdate)
        {
            PlayerHealthUpdate();
            PlayerSandUpdate();
            WaveCountUpdate();
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    public void EnableUIPanel(GameObject uiPanel)
    {
        uiPanel.SetActive(true);
    }

    public void DisableUIPanel(GameObject uiPanel)
    {
        uiPanel.SetActive(false);
        //Time.timeScale = 0.01f;
    }

    public void TimePause()
    {
        Time.timeScale = 0.01f;
    }

    public void TimeRestart()
    {
        Time.timeScale = 1.0f;

    }

    public void PlayerHealthUpdate()
    {
        playerHealth.text = GameManager.Instance.playerHealth.ToString();
    }

    public void PlayerSandUpdate()
    {
        playerSand.text = GameManager.Instance.playerSand.ToString();
    }

    public void WaveCountUpdate()
    {
        waveCount.text = GameManager.Instance.waveCount.ToString();
    }

    public void RestartGame()
    {
        GameManager.Instance.SetGameMode(GameManager.GameMode.Menu);
        SceneManager.LoadScene("mainH", LoadSceneMode.Single);
    }

    IEnumerator waitForCredits()
    {
        EnableUIPanel(creditsPanel);
        yield return new WaitForSeconds(5.0f);
        DisableUIPanel(creditsPanel);
    }
}