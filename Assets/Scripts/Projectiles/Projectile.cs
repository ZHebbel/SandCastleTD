﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
	public UnitBase target;
	public int mDamage;
	public float moveSpeed = 4f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (target != null)
		{
			if (target.gameObject.activeInHierarchy)
			{
				Vector3 newPostion = Vector3.MoveTowards(gameObject.transform.position, target.transform.position, moveSpeed * Time.deltaTime);
				gameObject.transform.position = newPostion;
				if (Vector3.Distance(target.transform.position, newPostion) < 0.1f)
				{
					Hit();
				}
			}else
			{
				ReturnToPool();
			}
			
		}else
		{
			Debug.Log("Something else already destroyed this target!");
			ReturnToPool();
		}
		
	}

	void Hit()
	{
		Debug.Log("Hit!!!");
		target.Health -= mDamage;
		ReturnToPool();
		
	}

	void ReturnToPool()
	{
		gameObject.transform.position = new Vector3(100, 100, 100);
		gameObject.SetActive(false);
	}
}
