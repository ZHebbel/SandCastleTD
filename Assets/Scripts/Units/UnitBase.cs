﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBase : MonoBehaviour {
	public int StartingHealth;
    public int Health;
    public float MoveSpeed;
    public Vector3 NextPosition;
    public int sandReward;

    protected int mDirection;
    protected bool mIsInCastle;
    protected Waypoint mWaypoint;
    protected UnitType mUnitType;
    protected Transform mPosition;
	//protected int mStartingHealth; //was going to use for deteriorating unit sprites

	public AudioSource unitAudioSource;
	public AudioClip spawnSound;
	public AudioClip moveSound;
	public AudioClip deathSound;

    protected Animator mAnimator;

    public enum UnitType
    {
        Droplet,
        Crab,
        Turtle
    }

    public void Awake()
    {
        //Sets the unit to the unit to the unit pool

        UnitManager.instance.RemoveUnit(this.gameObject);
        mAnimator = gameObject.GetComponent<Animator>();
        Reset();
    }


    public virtual void EnterCastle() { }
    public virtual void MoveTo(Vector3 position) { }
    public virtual void SetPosition(Vector3 position) { }
    public virtual void SetNextWaypoint(Waypoint nextWaypoint) { }
    public virtual void OnTriggerEnter2D(Collider2D collision) { }

	public void PlaySpawnSound()
	{
		unitAudioSource.PlayOneShot(spawnSound);
	}
	public void PlayMoveSound()
	{
		unitAudioSource.clip = moveSound;
		unitAudioSource.loop = true;
	}

	public void PlayDeathSound()
	{
		unitAudioSource.PlayOneShot(deathSound);
		unitAudioSource.loop = false;
	}

	protected void Die()
	{
		PlayDeathSound();
		UnitManager.instance.RemoveUnit(this.gameObject);
		UnitManager.instance.KillUnit();
		SoundManager.Instance.PlayKillUnit();
	}

	public void Reset()
	{
		Health = StartingHealth;
		mAnimator.SetInteger("HurtLevel", 0);
    }
}
