﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider healthBar;
    public UnitBase Unit;

    private Transform mUnitPosition;
    private float left;
    private float top;

    private Vector2 playerScreen;

    void Start()
    {
        mUnitPosition = Unit.gameObject.GetComponent<Transform>();
        healthBar.maxValue = Unit.Health;
    }

    void Update()
    {
        healthBar.value = Unit.Health;
        healthBar.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(mUnitPosition.position+new Vector3(0.0f, 0.4f, 0.0f));
    }
}

//Vector3 healthBarWorldPosition = target.transform.position + new Vector3(0.0f, target.height, 0.0f);
//healthBarScreenPosition = Camera.main.WorldToScreenPoint(healthBarWorldPosition);