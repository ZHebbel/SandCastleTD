﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectilePool : MonoBehaviour {
	private static ProjectilePool _instance;
	public static ProjectilePool Instance
	{
		get{
			return _instance;
		}
	}
	public List<GameObject> mudBalls;
	public List<GameObject> sandBalls;
	public List<GameObject> weedBalls;

	// Use this for initialization
	void Awake () {
		if (_instance == null)
		{
			_instance = this;
		}else
		{
			Destroy(this.gameObject);
		}
	}

    void Start()
    {

    }


    public void ActivateMudBall(Vector3 position, UnitBase target)
	{
		Debug.Log(position);
		for (int i = 0; i < mudBalls.Count; ++i)
		{
			if (!mudBalls[i].activeInHierarchy)
			{
				mudBalls[i].SetActive(true);
				mudBalls[i].GetComponent<Projectile>().target = target;
				mudBalls[i].transform.position = position;
				break;
			}
		}
	}

	public void ActivateSandBall(Vector3 position, UnitBase target)
	{
		Debug.Log(position);
		for (int i = 0; i < sandBalls.Count; ++i)
		{
			if (!sandBalls[i].activeInHierarchy)
			{
				sandBalls[i].SetActive(true);
				sandBalls[i].GetComponent<Projectile>().target = target;
				sandBalls[i].transform.position = position;
				break;
			}
		}
	}

	public void ActivateWeedBall(Vector3 position, UnitBase target)
	{
		Debug.Log(position);
		for (int i = 0; i < weedBalls.Count; ++i)
		{
			if (!weedBalls[i].activeInHierarchy)
			{
				weedBalls[i].SetActive(true);
				weedBalls[i].GetComponent<Projectile>().target = target;
				weedBalls[i].transform.position = position;
				break;
			}
		}
	}
}
